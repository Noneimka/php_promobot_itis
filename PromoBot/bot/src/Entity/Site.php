<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SiteRepository;
use DateTime;

/**
 * Site
 *
 * @ORM\Table(name="site", uniqueConstraints={@ORM\UniqueConstraint(name="site_site_key", columns={"site"})})
 * @ORM\Entity(repositoryClass=SiteRepository::class)
 */
class Site
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="site_id_seq", allocationSize=1, initialValue=1)
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="text", nullable=false)
     */
    private string $site;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private ?DateTime $lastUpdate;

    /**
     * @param string $site
     * @param int|null $id
     * @param DateTime|null $lastUpdate
     */
    public function __construct(string $site, int $id = null, ?DateTime $lastUpdate = null)
    {
        $this->id = $id;
        $this->site = $site;
        $this->site = $site;
//        if ($lastUpdate == null) {
//            $this->lastUpdate = new DateTime();
//        } else {
//            $this->lastUpdate = $lastUpdate;
//        }
        $this->lastUpdate=$lastUpdate;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite(string $site): void
    {
        $this->site = $site;
    }

    /**
     * @return DateTime|null
     */
    public function getLastUpdate(): ?DateTime
    {
        return $this->lastUpdate;
    }

    /**
     * @param DateTime|null $lastUpdate
     */
    public function setLastUpdate(?DateTime $lastUpdate): void
    {
        $this->lastUpdate = $lastUpdate;
    }
}
