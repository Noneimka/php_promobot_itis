<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PromocodesRepository;

/**
 * Promocodes
 *
 * @ORM\Table(name="promocodes", indexes={@ORM\Index(name="IDX_F2111250694309E4", columns={"site"})})
 * @ORM\Entity(repositoryClass=PromocodesRepository::class)
 */
class Promocodes
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="promocodes_id_seq", allocationSize=1, initialValue=1)
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="promocode", type="text", nullable=false)
     */
    private $promocode;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=false)
     */
    private $url;

    /**
     * @var Site
     *
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="site", referencedColumnName="id")
     * })
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="check_date", type="datetime", nullable=false)
     */
    private ?DateTime $check_date;

    /**
     * @param Site $site
     * @param string $promocode
     * @param string $url
     * @param string $description
     * @param DateTime|null $check_date
     * @param int|null $id
     */
    public function __construct(Site $site, string $promocode, string $url, string $description, ?DateTime $check_date = null, int $id = null)
    {
        $this->id = $id;
        $this->promocode = $promocode;
        $this->url = $url;
        $this->site = $site;
        $this->description = $description;
        $this->check_date = $check_date;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPromocode(): string
    {
        return $this->promocode;
    }

    /**
     * @param string $promocode
     */
    public function setPromocode(string $promocode): void
    {
        $this->promocode = $promocode;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return Site
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @param Site $site
     */
    public function setSite(Site $site): void
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DateTime|null
     */
    public function getCheckDate(): ?DateTime
    {
        return $this->check_date;
    }

    /**
     * @param DateTime|null $check_date
     */
    public function setCheckDate(?DateTime $check_date): void
    {
        $this->check_date = $check_date;
    }
}
