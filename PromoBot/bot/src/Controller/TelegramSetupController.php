<?php

namespace App\Controller;

use App\Service\TelegramService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Telegram\Bot\Exceptions\TelegramSDKException;

#[Route('/telegram/setup', name: 'telegram_setup')]
class TelegramSetupController extends AbstractController
{
    private TelegramService $telegram;

    /**
     * @throws TelegramSDKException
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->telegram = new TelegramService($manager);
        $this->telegram->getTelegram()->setWebhook(["url" => $_ENV['WEBHOOK_URL'] . "/getUpdate"]);
    }

    /**
     * @Route(methods="GET", name="Start")
     * @return Response
     * @throws TelegramSDKException
     */
    public function getTg(): Response
    {
        return new Response($this->telegram->getTelegram()->getMe());
    }

    /**
     * @Route(methods="POST", name="callback")
     * @param Request $request
     * @return Response
     */
    public function postTg(Request $request): Response
    {
        return new Response();
    }
}
