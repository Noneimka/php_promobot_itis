<?php

namespace App\Controller;

use App\Service\TelegramService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/getUpdate', name: 'update')]
class UpdateController extends AbstractController
{
    private TelegramService $telegramService;

    public function __construct(TelegramService $telegramService)
    {
        $this->telegramService = $telegramService;
    }

    /**
     * @Route(methods="POST", name="getUpdate")
     * @param Request $request
     * @return Response
     */
    public function getUpdate(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $this->telegramService->getMessageHandler()->handleMessage($data);

        return new Response();
    }
}
