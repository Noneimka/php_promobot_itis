<?php

namespace App\Service;

use App\Handler\MessageHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Dotenv\Dotenv;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class TelegramService
{
    private Api $telegram;
    private MessageHandler $messageHandler;

    public function __construct(EntityManagerInterface $manager) {
        $dir = dirname(__DIR__);
        (new Dotenv())->loadEnv(substr($dir, 0, strpos($dir, '\src')) . '/.env');
        try {
            $this->telegram = new Api($_ENV['BOT_TOKEN']);
        } catch
        (TelegramSDKException $e) {
            echo "NO TOKEN";
            $this->telegram = new Api();
        }
        $this->messageHandler = new MessageHandler($manager, $this->telegram);
    }

    /**
     * @return Api
     */
    public function getTelegram(): Api
    {
        return $this->telegram;
    }

    /**
     * @param Api $telegram
     */
    public function setTelegram(Api $telegram): void
    {
        $this->telegram = $telegram;
    }

    /**
     * @return MessageHandler
     */
    public function getMessageHandler(): MessageHandler
    {
        return $this->messageHandler;
    }

    /**
     * @param MessageHandler $messageHandler
     */
    public function setMessageHandler(MessageHandler $messageHandler): void
    {
        $this->messageHandler = $messageHandler;
    }


}