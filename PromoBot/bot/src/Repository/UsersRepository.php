<?php

namespace App\Repository;

use App\Entity\Site;
use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    public function getAllUsers(): array
    {
        return $this->findAll();
    }

    /**
     * @throws Exception
     */
    public function addUser(Users $user): array
    {
        $check = false;
        try {
            $conn = $this->getEntityManager()->getConnection();
            $conn->prepare("INSERT INTO users(user_id, chat_id) VALUES(:user_id, :chat_id)")
                ->executeQuery(["user_id" => $user->getUserId(), "chat_id" => $user->getChatId()]);
            $check = true;
        } catch (Exception $e) {
            return [$check, $this->findOneBy(["userId" => $user->getUserId()])];
        }
        $user->setId($conn->lastInsertId());
        return [$check, $user];
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function updateSite(int $id, Site $site)
    {
        $user = $this->find($id);
        $user->setSite($site);
        $this->getEntityManager()->flush();
    }
}