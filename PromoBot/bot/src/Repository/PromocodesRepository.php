<?php
namespace App\Repository;

use App\Entity\Promocodes;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

class PromocodesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Promocodes::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function addPromo(Promocodes $promo) {
        $date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        $promo->setCheckDate($date);
        $this->getEntityManager()->persist($promo);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function updateDateToNow(Promocodes $promo) {
        $date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        $promo->setCheckDate($date);
        $this->getEntityManager()->persist($promo);
        $this->getEntityManager()->flush();
    }
}