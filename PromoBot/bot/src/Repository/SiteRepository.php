<?php
namespace App\Repository;

use App\Entity\Site;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class SiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Site::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function AddSite(Site $site): Site
    {
        $this->getEntityManager()->persist($site);
        $this->getEntityManager()->flush();
        return $site;
    }

    public function update(Site $site) {
        $date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        $this->getEntityManager()->persist($site);
        $site->setLastUpdate($date);
        $this->getEntityManager()->flush();
    }
}