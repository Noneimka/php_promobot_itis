<?php

namespace App\Handler;

use App\Entity\Promocodes;
use App\Entity\Site;
use App\Service\BrowserService;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Safe\DateTime;
use Symfony\Component\DomCrawler\Crawler;

class PromoHandler
{
    private BrowserService $browser;
    private $promoUrl;
    private Site $site;
    private EntityRepository $promocodesRepository;
    private array $url;
    private array $promosites = [
        'promokod.pikabu.ru/shops/',
        'goodcoupon.ru/promokody/',
        'promokodus.com/campaigns/'
    ];

    /**
     * @param Site $site
     * @param EntityManagerInterface $manager
     */
    public function __construct(Site $site, EntityManagerInterface $manager)
    {
        $this->site = $site;
        $this->url = explode('.', $site->getSite());
        $this->promocodesRepository = $manager->getRepository(Promocodes::class);
        $this->browser = new BrowserService();
    }

    public function checkPromo()
    {
        for ($i = 0; $i <= 2; $i++) {
            $link = $this->promosites[$i];
            $urlSheme = $this->url;
            $link = $link . $urlSheme[0];
            $context = stream_context_create(
                array(
                    "http" => array(
                        "header" => ["User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36",
                            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"]
                    )
                )
            );

            if ($this->isSiteAvailable('https://' . $link)) {
                $html = file_get_contents('https://' . $link, false, $context);
            } elseif ($this->isSiteAvailable('https://' . $link . "-" . $urlSheme[1])) {
                $html = file_get_contents('https://' . $link . "-" . $urlSheme[1], false, $context);
            } else {
                continue;
            }

            $crawler = new Crawler(null, $link);
            $crawler->addHtmlContent($html);
            switch ($i) {
                case 0:
                    $promoUrl = ($crawler->filter('div.open-tovar')->children('a')
                        ->each(function (Crawler $node) {
                            return $node->attr('href');
                        }));
                    $this->promoPikabu('https://' . $link, $promoUrl);
                    break;
                case 1:
                    $promoUrl = ($crawler->filter('div.type-coupon')
                        ->each(function (Crawler $node) {
                            if (str_contains($node->attr('class'), 'status-unreliable') !== true) {
                                return "?c=" . substr($node->attr('id'), 5);
                            }
                            return '';
                        }));
                    $this->promoGoodCoupon('https://' . $link, $promoUrl);
                    break;
                case 2:
                    $promoUrl = ($crawler->filter('div.coupons-horizontal')->children('div.coupon')
                        ->each(function (Crawler $node) {
                            if (!str_contains($node->attr('class'), 'coupon-cross')) {
                                return "?couponId=" . $node->attr('data-id') . "&display_code=1";
                            }
                            return '';
                        }));
                    $promoUrl = array_diff($promoUrl, array(''));
                    $this->promoPromokodus('https://' . $link, $promoUrl);
                    break;
            }
        }
        $this->browser->terminate();
    }

    private function promoPikabu($base, $promoId)
    {
        foreach ($promoId as $id) {
            $this->promoUrl = ($base . $id);
            if ($this->updateIfExsist($this->promoUrl)) {
                continue;
            }
            $response = $this->browser->getClient()->get($this->promoUrl);
            $response->getCrawler()->filter('div.coupon-popup__content')
                ->each(function (Crawler $node) {
                    try {
                        $desc = $node->children('a.coupon-popup__title')->text();
                        $code = $node->children('div.coupon-popup__promocode-wrapper > div.coupon-popup__promocode-value')->text();
                        $code = trim($code);
                        $desc = trim($desc);
                    } catch (\Exception) {
                        return '';
                    }
                    if ($code != 'СМОТРИ НА САЙТЕ') {
                        $promo = new Promocodes($this->site, $code, $this->promoUrl, $desc);
                        $this->promocodesRepository->addPromo($promo);
                        return $code;
                    }
                    return '';
                });
            $this->browser->getClient()->get('https://example.org/');
        }
    }

    private function promoGoodCoupon($base, $promoId)
    {
        foreach ($promoId as $id) {
            $this->promoUrl = ($base . $id);
            if ($this->updateIfExsist($this->promoUrl)) {
                continue;
            }
            $post = "post-" . substr($id, 3);
            $response = $this->browser->getClient()->get($this->promoUrl);
            $response->getCrawler()->filter("div.$post")
                ->each(function (Crawler $node) {
                    try {
                        $desc = $node->filter('a.coupon-button')->first()->text();
                        $code = $node->filter('a.coupon-code-link')->first()->attr('data-rel');
                        $code = trim($code);
                        $desc = trim($desc);
                    } catch (\Exception $e) {
                        return '';
                    }
                    if (!str_contains($code, 'НА САЙТЕ') and $code != 'НЕ НУЖЕН') {
                        $promo = new Promocodes($this->site, $code, $this->promoUrl, $desc);
                        $this->promocodesRepository->addPromo($promo);
                        return $code;
                    } else {
                        return '';
                    }
                });
            $this->browser->getClient()->get('https://example.org/');
        }
    }

    private function promoPromokodus($base, $promoId)
    {
        foreach ($promoId as $id) {
            $this->promoUrl = ($base . $id);
            if ($this->updateIfExsist($this->promoUrl)) {
                continue;
            }
            $response = $this->browser->getClient()->get($this->promoUrl);
            $response->getCrawler()->filter('div#coupon-modal')
                ->each(function (Crawler $node) {
                    try {
                        $desc = $node->children("div.cm-top > div.text > h4.title")->text();
                        $code = $node->children('div.cm-slider > div.slick-list > div.slick-track > div.slick-slide > div.cm-inner > div.cm-code-wrap > input')->attr('value');
                        $code = trim($code);
                        $desc = trim($desc);
                    } catch (\Exception $e) {
                        return '';
                    }
                    if ($code != 'ПОЛУЧИТЕ В ПИСЬМЕ') {
                        $promo = new Promocodes($this->site, $code, $this->promoUrl, $desc);
                        $this->promocodesRepository->addPromo($promo);
                        return $code;
                    } else {
                        return '';
                    }
                });
            $this->browser->getClient()->get('https://example.org/');
        }
    }

    function isSiteAvailable($url): bool
    {
        $headers = array("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpcode == 200 or $httpcode == 403) {
            return true;
        }
        return false;
    }

    private function updateIfExsist($promoUrl) : bool{
        $check = $this->promocodesRepository->findOneBy(["url" => $promoUrl]);
        if ($check != null) {
            $this->promocodesRepository->updateDateToNow($check);
            return true;
        } else return false;
    }
}