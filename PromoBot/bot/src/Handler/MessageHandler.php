<?php

namespace App\Handler;

use App\Entity\Promocodes;
use App\Entity\Site;
use App\Entity\Users;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class MessageHandler
{
    private EntityManagerInterface $manager;
    private Api $telegram;

    public function __construct(EntityManagerInterface $entityManager, Api $telegram)
    {
        $this->manager = $entityManager;
        $this->telegram = $telegram;
    }

    /**
     * @throws TelegramSDKException
     */
    public function handleMessage($update)
    {
        $usersRepository = $this->manager->getRepository(Users::class);
        $siteRepository = $this->manager->getRepository(Site::class);
        $promocodesRepository = $this->manager->getRepository(Promocodes::class);

        if (array_key_exists('message', $update) == 1) {
            $text = $update['message']['text'];
            $chat_id = $update['message']['chat']['id'];
            $user_id = $update['message']['from']['id'];
            $msg = 'Секундочку';
            if (str_contains($text, '/start') === true) {

                $user = new Users($user_id, $chat_id);
                $check = $usersRepository->addUser($user);

                if ($check[0]) {
                    $msg = "Добро пожаловать, новый друг!\n";

                } else {
                    $msg = "С возвращением!\n";
                }
                $msg = $msg . "Список команд: \n/newsite (Example: /newsite kpfu.ru) \n/mysite \n/getpromo";
            } elseif
            (str_contains($text, '/newsite') === true) {
                $text = trim($text);
                $words = explode(' ', $text);
                if (sizeof($words) > 1) {
                    $user = $usersRepository->findOneBy(['userId' => $user_id]);
                    if ($this->isSiteAvailable($words[1])) {
                        $url = $this->parseUrl($words[1]);
                        $site = $siteRepository->findOneBy(["site" => $url]);
                        if ($site == null) {
                            $site = $siteRepository->AddSite(new Site($url, null, null));
                        }
                        $usersRepository->updateSite($user->getId(), $site);
                        $msg = "Сайт успешно обновлён!";
                    } else {
                        $msg = "Сайт не доступен, проверьте корректонсть URL";
                    }
                } else {
                    $msg = "Что-то пошло не поплану, попробуй ещё раз! \n Example: /newsite kpfu.ru";
                }
            } elseif
            (str_contains($text, '/mysite') === true) {
                $user = $usersRepository->findOneBy(['userId' => $user_id]);
                if ($user->getSite() != null) {
                    $site = $user->getSite();
                    $msg = "Ваш текущий сайт: " . $site->getSite();
                } else {
                    $msg = "Вы ещё не выбрали сайт!\n/newsite (Example: /newsite kpfu.ru";
                }
            } elseif
            (str_contains($text, '/getpromo') === true) {
                $user = $usersRepository->findOneBy(['userId' => $user_id]);
                $site = $user->getSite();
                $date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
                $date = $date->format('Y/m/d');
                if ($site != null) {
                    if ($site->getLastUpdate() == null || $site->getLastUpdate()->format('Y/m/d') != $date) {
                        $promo = new PromoHandler($site, $this->manager);
                        $promo->checkPromo();
                        $siteRepository->update($site);
                    }
                    $allPromocodes = $promocodesRepository->findBy(["site" => $site]);
                    if ($allPromocodes != null) {
                        $msg = '';
                        foreach ($allPromocodes as $promo) {
                            if ($promo->getCheckDate()->format('Y/m/d') != $date) {
                                $this->manager->remove($promo);
                                $this->manager->flush();
                                continue;
                            }
                            if (!str_contains($msg, $promo->getPromocode())) {
                                $msg = $msg . $promo->getPromocode() . " - " . $promo->getDescription() . "\n";
                            }
                        }
                    } else {
                        $msg = 'Промокоды не найдены';
                    }
                } else {
                    $msg = "Вы ещё не выбрали сайт!\n/newsite (Example: /newsite kpfu.ru";
                }
            }
            $this->telegram->sendMessage(['chat_id' => $chat_id, 'text' => $msg]);
        }
    }

    function isSiteAvailable($url, $check = 0): bool
    {
        $user_agent = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36';

        if ($check == 2) {
            return false;
        }

        if (str_contains($url, 'https://') or str_contains($url, 'http://')) {

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_VERBOSE, false);

            $response = curl_exec($curl);
            curl_close($curl);
            $res = (bool)($response);
            if ($res) {
                return true;
            } elseif (str_contains($url, 'https://')) {
                return $this->isSiteAvailable("http" . substr($url, 5), $check + 1);
            } else {
                return $this->isSiteAvailable("https" . substr($url, 4), $check + 1);
            }
        } else
            return $this->isSiteAvailable("https://" . $url, $check + 1);
    }

    public function parseUrl(string $url): string
    {
        $word = parse_url(trim(strtolower($url), "/ \t\n\r\0\x0B"));
        if (array_key_exists('host', $word)) {
            $word = explode('.', $word['host']);
        } else {
            $word = explode('.', $word['path']);
        }

        if ($word[0] == 'www') {
            return $word[1] . "." . $word[2];
        } else {
            return $word[0] . "." . $word[1];
        }
    }
}